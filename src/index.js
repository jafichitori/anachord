import rmRepeatNotes from './anachord_modules/anachordRmRepeatNotes'
import acomodoNombres from './anachord_modules/anachordAcomodoNombres'
import invs from './anachord_modules/anachordInversiones'
import anaOne from './anachord_modules/anachordAnaOne'
import Acorde from './anachord_modules/anachordAcordeConstructor'
import { textoFiltrado, esToIs } from './std'

export default function analisis(chord,all_intervalos=false) {

    let nombres_retorno;

    console.group('ConsChord | AnaChord');
    try {

        // validacion y conversion de datos
        if (!Array.isArray(chord) && typeof chord != typeof '')
            throw 'argument is not an Array or String';

        if (typeof chord == typeof '') chord = chord.split(',');

        if (!chord.every(note => /^(([a-g](is|es)?)|es)(:\d+)?$/g.test(note)))
            throw 'armuments are not a list of musical notes';

            // verificación de bemoles a sostenidos y fis y bis
        chord = rmRepeatNotes( chord ).map( note =>
              (note.includes('eis'))
                ? note.includes(':')
                    ? 'f:'+note.split(':')[1]
                    : 'f'
            : (note.includes('bis'))
                ? note.includes(':')
                    ? 'c:'+note.split(':')[1]
                    : 'c'
            : (note.includes('es'))
                ? note.includes(':')
                    ? `${esToIs(note.split(':')[0])}:${note.split(':')[1]}`
                    : esToIs(note)
            : note
        );





        if (all_intervalos) {
            // acomodar nombres segun su longitud
            nombres_retorno = acomodoNombres(
                // get all invs
                invs( chord )
                    // por cada inversion
                    // crear objeto acorde
                    // analizar
                    // y adaptar el nombre a formato humano
                    .map( item => textoFiltrado( anaOne( new Acorde( item ) ) ) )
                    // filtrar nombres extranos
                    .filter( item => !item.includes('?') )
            );

        } else {
            let tmp = textoFiltrado( anaOne( new Acorde( chord ) ) )
            if (!tmp.includes('?')) nombres_retorno = tmp;
            else nombres_retorno = '?';
        }
    }

    catch (e) { throw new Error(e); }
    finally { console.groupEnd(); }

    return nombres_retorno;
}
