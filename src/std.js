/**
* @fileoverview standard ConsChord's functions , define a group of standard functions to use by ChosCord's librarys
*
* @vrsion 2.1
*
* @author Jesus Figueredo <jafigueredo@gmail.com>
*
* this version is rewrite with js module system
*/


import { NOTE_NAMES } from './anachord_modules/defs'

/**
* convert music note names from analitic name
* (using prefixes is to charp and es to flat) to
* human readible name (♯ and/or ♭).
*
* @param {string} name of musical note in analytic mode
* @return {string} name of musical note in human readable mode
*/

export const textoFiltrado = name =>
    name.includes('is') ? name.replace('is', '♯') :
    name.includes('es') ? name.replace('es', '♭') :
    name ;





/**
* traslate the music notes from flat note to sharp note
* @param {string} nota
* @return {string}
*/
export const esToIs = nota =>
    (nota == 'ces')
        ? 'b' :
    (nota.includes("es"))
        ? NOTE_NAMES[ NOTE_NAMES.indexOf(nota[0])-1]
        : nota;




/**
* Verifies if the string is a valid musical note name
* @param {string} note
* @return {boolean}
*/
export const permitedNotes = note => (NOTE_NAMES.includes(note) || note === 'x')
// function permitedNotes(note) {
//     if (NOTE_NAMES.includes(note) || note === 'x') {
//         return true;
//     }
//     return false;
// }






/**
* return the name of the musical note by pulsed fret of a string of an instrument
* @param {string} texto_calc pair of string_tuning:pulsed_fret
* @return {string} name of the musical note
*/
export const noteByFret = (string, fret) => {
    if (fret == -1) return 'x';
    if (permitedNotes(string) ) {
        return NOTE_NAMES[(
            ( NOTE_NAMES.indexOf(string) + fret )
                % NOTE_NAMES.length
            )];
    } else {
        throw 'incorrect note name';
    }

}

/**
retorna el traste correspondiente al nombre de la nota en una cuerda dada
* @param {string, string}
* @return {number} [-1..]
*/
export const fretByNote = (string, note) => {

    if (note == 'x') return -1;

    var indice = NOTE_NAMES.indexOf(string);
    var final =  NOTE_NAMES.indexOf(note);


    return (final < indice)
        ? ((final + NOTE_NAMES.length) - indice)
        : (final - indice);
}
