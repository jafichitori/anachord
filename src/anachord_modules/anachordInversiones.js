import acomodo from './anachordAcomodo'

export default function invs(acorde) {
    let inv = [];
    acorde = acomodo( [...acorde] );

    // for (i = 0; i < acorde.length+1;i++) {
    acorde.forEach( () => {
        acorde.push(
            (() => {
                let n = acorde.shift(1,1);

                if (n.includes(':')) {
                    let tmp = n.split(':');
                    n = `${tmp[0]}:${++tmp[1]}`;
                }

                return n;
            })()

        );
        acorde = acomodo(acorde);
        inv.push( acomodo( [...acorde]) );
    });
    // }

    return inv;
}
