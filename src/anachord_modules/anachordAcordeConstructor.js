import getIntervalos from './anachordGetIntervalos'

export default function Acorde(notas) {
    this.composicion = [...notas];
    this.base = notas[0];
    this.intervalos = getIntervalos([...notas]);
}
