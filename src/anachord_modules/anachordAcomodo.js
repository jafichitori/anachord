export default function acomodo(array_acorde) {
    var notas = [...array_acorde];
    for (let i = 0; i < notas.length; i++) {
        if (i != notas.length-1 && notas[i].split(':')[1] > notas[i+1].split(':')[1]) {
            let tmp = notas[i];
            notas[i] = notas[i+1];
            notas[i+1] = tmp;
            i = 0;

        } else if (i != 0 && notas[i].split(':')[1] < notas[i-1].split(':')[1]) {
            let tmp = notas[i];
            notas[i] = notas[i-1];
            notas[i-1] = tmp;
            i = 0;

        } else {
            continue;
        }
    }
    return notas;
}
