/**
* @fileoverview standard ConsChord's constants , define a group of constants to use by ChosCord's librarys
*
* @vrsion 2.1
*
* @author Jesus Figueredo <jafigueredo@gmail.com>
*
* this version is rewrite with js module system
*/


/**
*  define the music note names in scale musical
* @const {array}
*/
export const NOTE_NAMES = [ 'c', 'cis', 'd', 'dis', 'e', 'f', 'fis', 'g', 'gis', 'a', 'ais', 'b', ];


/**
* define musical alterations in analytic mode
* @const {array}
*/
export const ACCIDENTAL = ["is","es"];


/**
* define natural musical note names
* @const {array}
*/
export const NATURAL_NOTES_NAMES = ["c","d","e","f","g","a","b"];


/**
* define chord names types
* @const {array}
*/
export const TRIAD_NAMES = [ 'M', 'm', 'disminuido', 'aumentado', 'm aug5', '2sus', '4sus', '5' ];


/**
* define interval names from chords
* @const {array}
* @deprecated
*/
export const POS_TRIAD_NAMES = [ 'min2', 'add2', 'add4', 'min6', '6', '7m', 'maj7' ];
