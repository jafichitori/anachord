/**
reacomodo de los valores (nombres) con tamañs de menor a mayor
@param {Array} array_nombres
@return {Array} de nombres
*/
export default function acomodoNombres(array_nombres) {
    if (array_nombres.length == 0) return ['?'];
    let texto = [...array_nombres].map(item=>item.trim());
    console.log(texto);
    // texto = texto.map(item => item.trim());
    for (let i = 0; i < texto.length; i++) {
        if (i != texto.length-1 && texto[i].split(' ').length > texto[i+1].split(' ').length) {
            console.info(texto[i],'|',texto[i+1],i);
            let tmp = texto[i];
            texto[i] = texto[i+1];
            texto[i+1] = tmp;
            i = -1;

        } else if ( i > 0 && texto[i].split(' ').length < texto[i-1].split(' ').length) {
            console.warn(texto[i],i,'o');
            let tmp = texto[i];
            texto[i] = texto[i-1];
            texto[i-1] = tmp;
            i = -1;

        } else {
            continue;
        }
    }

    console.info(texto);

    // reordenar de menor a mayor (con igual cantidad de partes)
    for (let i = 0; i < texto.length; i++) {

        if (i != texto.length-1 && texto[i].split(' ').length == texto[i+1].split(' ').length) {
            let cmp = [0,0];
            let vl = [texto[i].split(' '), texto[i+1].split(' ')];
            for (let o = 0; o < vl[0].length; o++) {
                cmp[0] += vl[0][o].length;
                cmp[1] += vl[1][o].length;
            }

            if (cmp[0] > cmp[1]) {
                let tmp = texto[i];
                texto[i] = texto[i+1];
                texto[i+1] = tmp;
                i = -1;
            }

        } else if (i != 0 && texto[i].split(' ').length == texto[i-1].split(' ').length) {
           let cmp = [0,0];
           let vl = [texto[i].split(' '), texto[i-1].split(' ')];
           for (let o = 0; o < vl[0].length; o++) {
               cmp[0] += vl[0][o].length;
               cmp[1] += vl[1][o].length;
           }

           if (cmp[0] < cmp[1]) {
               let tmp = texto[i];
               texto[i] = texto[i-1];
               texto[i-1] = tmp;
               i = -1;
           }

       }  else {
           continue;
       }

    }
    console.info(texto);
    return texto;
}
