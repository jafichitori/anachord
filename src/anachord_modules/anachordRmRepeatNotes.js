export default function rmRepeatNotes(acorde) {
    for (let a = 0; a < acorde.length; a++) {
        for (let b = 0; b < acorde.length; b++) {
            if (a==b) { continue; }
            if (acorde[a] == acorde[b]) {
                acorde.splice(b,1);
                break;
            }
        }
    }

    return acorde;

}
