import { NOTE_NAMES } from './defs'
/**
@param {Array} notes notas a analizar
@return {array} intervalos
*/
const getIntervalos = (notes) => notes.map((n) => tonesToInterval_(notes[0],n) );

/*devuelve el intervalo ascendente que se encuetra entre 'nota' desde 'nota_ref'*/
function tonesToInterval_ (nota_ref,nota) {

    function octavas( nota_ref, nota ) {

        let octava_ref = nota_ref.split(':')[1],
            octava_ult = nota.split(':')[1],
            total_oct_dist = octava_ult - octava_ref;

        nota_ref = nota_ref.split(':')[0];
        nota = nota.split(':')[0]


        if (nota === nota_ref) return total_oct_dist;
        if (total_oct_dist == 0) return 0;

        let u = octava_ref;
        let o = octava_ref
        for (let i = NOTE_NAMES.indexOf(nota_ref);; i++) {

            if (i >= NOTE_NAMES.length) {
                u++; i = 0;
            }

            if (NOTE_NAMES[i] == nota && u == octava_ult) break;

            if (NOTE_NAMES[i] == nota_ref && u > octava_ref) o++;

        }

        return o-octava_ref;
    }

    function defIntervalo(indice_semitonos) {
        const comps = [
            '1j',    // u 1
            '2m', '2M',  // 2
            '3m','3M',  // 3
            '4j','4a',  // 4
            '5j',    // 5
            '6m','6M',  // 6
            '7m','7M'   // 7
        ];

        let octavas = parseInt(indice_semitonos/12);
        let semitonos = indice_semitonos%12;


        let intervalo = comps[semitonos].split('');
        intervalo[0] = parseInt(intervalo[0])+Number(octavas)*7;
        return intervalo[0]+intervalo[1];

    }

    // si las notas poseen algún indicativo de octava nota:octava
    if (/[a-g]((is|es)?):\d+/g.test(nota)) {

        let n = nota.split(':')[0],
            n_ref,
            distancia
        // var distancia;

        // si nota_ref poseen algún indicativo de octava nota:octava
        if (/[a-g]((is|es)?):\d+/g.test(nota_ref)) {
            n_ref = nota_ref.split(':')[0]
            distancia = octavas(nota_ref, nota);
        } else {
            n_ref = nota_ref;
            distancia = octavas(nota_ref+':0',nota);
        }

        let index_nota_ref = NOTE_NAMES.indexOf(n_ref),
            index_nota = NOTE_NAMES.indexOf(n);

        if (index_nota < index_nota_ref) index_nota += 12;

        return defIntervalo((index_nota - index_nota_ref)+(12*distancia));

    } else { // se interpreta que está dentro de la misma octava
        let index_nota_ref = NOTE_NAMES.indexOf(nota_ref);
        let index_nota = NOTE_NAMES.indexOf(nota);
        if (index_nota < index_nota_ref) {
            index_nota += 12;
        }
        return defIntervalo(index_nota - index_nota_ref);
    }

    // return distancia;
}

export default getIntervalos
