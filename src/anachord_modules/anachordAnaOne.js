
export default function anaOne(obj) {

    const salidaPostTriada = nombre => nombre ? ` ${nombre}` : '';
    // analisa la triada del acorde
    const analisistriada = (estado, contador) => (
            // triada mayor
            ( estado.includes('3M') && estado.includes('5j') ) ?
                ((contador[0]+=2) && "" ) :

            // triada menor
            ( estado.includes('3m') && estado.includes('5j') ) ?
                ((contador[0]+=2) && "m" ) :

            // triada disminuida
            ( estado.includes('3m') && estado.includes('4a') ) ?
                ((contador[0]+=2) && String.fromCharCode(9675) ) :

            // triada aumentada
            ( estado.includes('3M') && estado.includes('6m') && !estado.includes('5j')) ?
                // ` ${estado.includes('7m') ? (String.fromCharCode(9839)+'5') : '+'}`
                ( (contador[0]+=2) &&
                " " + (estado.includes('7m') ? (String.fromCharCode(9839)+'5') : '+') ) :

            // triada menor con la 5 aumentada
            ( estado.includes('3m') && estado.includes('6m')  && !estado.includes('5j')  ) ?
                ((contador[0]+=2) &&
                "m " + (estado.includes('7m') ? (String.fromCharCode(9839)+'5') : 'aug5') ):

            // segunda suspendida con o sin la 5j
            ( estado.includes('2M') && !estado.includes('3M') ) ?
            ( (contador[0]+=2) && ' sus2' ) :

            // cuarta suspendida con o sin la 5j
            ( estado.includes('4j') && !estado.includes('3M') ) ?
                ( (contador[0]+=2) && ' sus4' ) :

            // cuarta aumentada suspendida
            ( estado.includes('3M') && estado.includes('4a') && !estado.includes('5j') ) ?
                ( (contador[0]+=2) && ' sus'+ String.fromCharCode(9839) +'4 3' ) :

            // acordes de potencia
            ( estado.includes('5j') && (estado.length == 2) ) ?
            ( (contador[0]++) && ' 5' ) :

            (() => {
                // throw 'valor en funcion analisis triada no aceptada';
                return '?';
            })()
        );
    // agrega los intervalos faltantes al nombre (en la primera octava)
    const posTriada = (intervalos, contador) => (

        // segunda menor
        salidaPostTriada(
            intervalos.includes('2m') &&
            intervalos.includes('5j') &&
            (contador[0]++) &&
            'min2'
        )
        // triadas normales con la segunda mayor
        + salidaPostTriada(
            intervalos.includes('2M') &&
            ( intervalos.includes('3M') || intervalos.includes('3m') ) &&
            (contador[0]++) &&
            'add2'
        )
        // triadas normales con la cuarta justa
        + salidaPostTriada(
            intervalos.includes('4j') &&
            ( intervalos.includes('3M') || intervalos.includes('3m') ) &&
            (contador[0]++) &&
            'add4'
        )
        // sesta mayores o septimas disminuidas
        + salidaPostTriada(
            intervalos.includes('6M') && (contador[0]++) && '6'
        )
        // sestas menores
        + salidaPostTriada(
            intervalos.includes('6m')  &&
            intervalos.includes('5j') &&
            (contador[0]++) &&
            'min6'
        )
        // septimas Mayores
        + salidaPostTriada(
            intervalos.includes('7M') &&
            intervalos.includes('3M') &&
            (contador[0]++) &&
            (intervalos.includes('5j') ?
                String.fromCharCode(9651) : 'maj7')
        )
        // septimas menores
        + salidaPostTriada(
            intervalos.includes('7m') && (contador[0]++) && '7'
        )
    );
    // agrega los intervalos faltantes al nombre a partir de la segunda octava
    const pt8vaAmpliada = (array_intervalos, contador) => ' ' +
        array_intervalos.filter(item => parseInt(item) > 7)
            .map(
                item => /\d+m$/g.test(item) ? ((contador[0]++) && `min${parseInt(item)}`) :   // menor
                        /\d+[Mj]$/g.test(item) ? ((contador[0]++) && `${parseInt(item)}`) :   // mayor o justa
                        /\d+a$/g.test(item) ? ((contador[0]++) && `aug${parseInt(item)}`) :   // aumentada
                        /\d+d$/g.test(item) ? ((contador[0]++) && `dim${parseInt(item)}`) :   // disminuida
                        ''
            ).join(' ');

    let nombre_salida = [];

    let conteo_intervalos_analisados = [1];
    let nombre =
        obj.base.split(':')[0] +                                             // nota base del acorde
        analisistriada(obj.intervalos, conteo_intervalos_analisados) +       // intervalos de la triada
        posTriada(obj.intervalos, conteo_intervalos_analisados) +            // intervalos de la primera octava
        pt8vaAmpliada(obj.intervalos, conteo_intervalos_analisados);          // demás intervalos
    // console.log(conteo_intervalos_analisados[0]);
    // console.warn(nombre, obj);
    if (obj.intervalos.length == conteo_intervalos_analisados[0])
        nombre_salida = nombre.trim();
    else nombre_salida = '?';



  return nombre_salida;
}
