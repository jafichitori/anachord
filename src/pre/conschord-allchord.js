function allChords(afinacion_p, acorde_p, cant_trastes_p) {
    let cant_cuerdas = afinacion_p.length;

    function trastes(afinacion, acorde, cant_trastes) {
        let indice = DEF_NOMBRE_NOTAS.indexOf(afinacion);
        let trastes = [];

        for(let i = 0; i < cant_trastes; i++) {

            let nota =  DEF_NOMBRE_NOTAS[(i + indice)%12];

            if ( acorde.includes(nota) ) {

                trastes.push({
                    traste: i,
                    nota: nota
                });
            }
        }
        return trastes;
    }

    const coleccion = (afinacion, acorde, cant_trastes) =>
        afinacion.map(item => trastes(item, acorde, cant_trastes));

    function acordes(afinacion, diapason) {
        let acordes = [];
        function functionName(cuerda=0, ...items) {

            for (item of diapason[cuerda]) {
                if (cuerda < cant_cuerdas-1) {
                    functionName(cuerda+1, ...items, item);
                } else {
                    acordes.push([
                    ...items,
                    item
                    ])
                }
            }
        }
        //
        functionName();



      return acordes;
    }

    function prueba(chr, ac) {
        let comp = ac.map(item => item.nota);

        let vf = chr.every(function(item) {
            return comp.includes(item);
        });

        let trsts = ac.map(item => item.traste);
        let mayor = trsts[0];
        let menor = trsts[0];

        for (trt of trsts) {
            if (trt == 0 ) continue;
            if (trt < menor) menor = trt;
            if (trt > mayor) mayor = trt;
        }

        // si la amplitud del acorde es menor que 5 trastes
        if (( mayor - menor + 1 ) > 5) {
            return false;
        }

        return vf;
    }

    acorde_p = esToIs(acorde_p);
    afinacion_p = esToIs(afinacion_p);

    let chrds = acordes(afinacion_p,  (() => {
        let tmp = coleccion(afinacion_p, acorde_p, cant_trastes_p);
        tmp.forEach( item => item.push( { traste: -1, nota: "x" }))
        console.warn(tmp);
        return tmp;
    })()
    );
    let chrds_acept = [];

    for (item of chrds) {
        if (prueba(acorde_p, item))
            chrds_acept.push(item);
    }


    return chrds_acept;

}
