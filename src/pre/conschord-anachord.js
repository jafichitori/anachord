const anachord = (() => {

/**
@param {Array} notes notas a analizar
@return {array} intervalos
*/
const getIntervalos = (notes) => notes.map((n) => tonesToInterval_(notes[0],n) );

/*devuelve el intervalo ascendente que se encuetra entre 'nota' desde 'nota_ref'*/
function tonesToInterval_ (nota_ref,nota) {

    function octavas( nota_ref, nota ) {

        var octava_ref = nota_ref.split(':')[1],
            octava_ult = nota.split(':')[1],
            total_oct_dist = octava_ult - octava_ref;

        nota_ref = nota_ref.split(':')[0];
        nota = nota.split(':')[0]


        if (nota === nota_ref) return total_oct_dist;
        if (total_oct_dist == 0) return 0;

        var u = octava_ref;
        for (var i = DEF_NOMBRE_NOTAS.indexOf(nota_ref), o = octava_ref;; i++) {

            if (i >= DEF_NOMBRE_NOTAS.length) {
                u++; i = 0;
            }

            if (DEF_NOMBRE_NOTAS[i] == nota && u == octava_ult) break;

            if (DEF_NOMBRE_NOTAS[i] == nota_ref && u > octava_ref) o++;

        }

        return o-octava_ref;
    }

    function defIntervalo(indice_semitonos) {
        let intervals = [];
        const comps = [
            '1j',    // u 1
            '2m', '2M',  // 2
            '3m','3M',  // 3
            '4j','4a',  // 4
            '5j',    // 5
            '6m','6M',  // 6
            '7m','7M'   // 7
        ];

        let octavas = parseInt(indice_semitonos/12);
        let semitonos = indice_semitonos%12;


        let intervalo = comps[semitonos].split('');
        intervalo[0] = parseInt(intervalo[0])+Number(octavas)*7;
        return intervalo[0]+intervalo[1];

    }

    // si las notas poseen algún indicativo de octava nota:octava
    if (/[a-g]((is|es)?)\:\d+/g.test(nota)) {

        var n = nota.split(':')[0],
            n_ref,
            distancia;

        // si nota_ref poseen algún indicativo de octava nota:octava
        if (/[a-g]((is|es)?)\:\d+/g.test(nota_ref)) {
            n_ref = nota_ref.split(':')[0]
            distancia = octavas(nota_ref, nota);
        } else {
            n_ref = nota_ref;
            distancia = octavas(nota_ref+':0',nota);
        }

        var index_nota_ref = DEF_NOMBRE_NOTAS.indexOf(n_ref),
            index_nota = DEF_NOMBRE_NOTAS.indexOf(n);

        if (index_nota < index_nota_ref) index_nota += 12;

        return defIntervalo((index_nota - index_nota_ref)+(12*distancia));

    } else { // se interpreta que está dentro de la misma octava
        var index_nota_ref = DEF_NOMBRE_NOTAS.indexOf(nota_ref);
        var index_nota = DEF_NOMBRE_NOTAS.indexOf(nota);
        if (index_nota < index_nota_ref) {
            index_nota += 12;
        }
        return defIntervalo(index_nota - index_nota_ref);
    }

    return distancia;
}

function invs(acorde) {
    let inv = [];
    acorde = acomodo( [...acorde] );

    // for (i = 0; i < acorde.length+1;i++) {
    acorde.forEach( () => {
        acorde.push(
            (() => {
                let n = acorde.shift(1,1);

                if (n.includes(':')) {
                    let tmp = n.split(':');
                    n = `${tmp[0]}:${++tmp[1]}`;
                }

                return n;
            })()

        );
        acorde = acomodo(acorde);
        inv.push( acomodo( [...acorde]) );
    });
    // }

    return inv;
}

function anaOne(obj) {

    const salidaPostTriada = nombre => nombre ? ` ${nombre}` : '';
    // analisa la triada del acorde
    const analisistriada = (estado, contador) => (
            // triada mayor
            ( estado.includes('3M') && estado.includes('5j') ) ?
                ((contador[0]+=2) && "" ) :

            // triada menor
            ( estado.includes('3m') && estado.includes('5j') ) ?
                ((contador[0]+=2) && "m" ) :

            // triada disminuida
            ( estado.includes('3m') && estado.includes('4a') ) ?
                ((contador[0]+=2) && String.fromCharCode(9675) ) :

            // triada aumentada
            ( estado.includes('3M') && estado.includes('6m') && !estado.includes('5j')) ?
                // ` ${estado.includes('7m') ? (String.fromCharCode(9839)+'5') : '+'}`
                ( (contador[0]+=2) &&
                " " + (estado.includes('7m') ? (String.fromCharCode(9839)+'5') : '+') ) :

            // triada menor con la 5 aumentada
            ( estado.includes('3m') && estado.includes('6m')  && !estado.includes('5j')  ) ?
                ((contador[0]+=2) &&
                "m " + (estado.includes('7m') ? (String.fromCharCode(9839)+'5') : 'aug5') ):

            // segunda suspendida con o sin la 5j
            ( estado.includes('2M') && !estado.includes('3M') ) ?
            ( (contador[0]+=2) && ' sus2' ) :

            // cuarta suspendida con o sin la 5j
            ( estado.includes('4j') && !estado.includes('3M') ) ?
                ( (contador[0]+=2) && ' sus4' ) :

            // cuarta aumentada suspendida
            ( estado.includes('3M') && estado.includes('4a') && !estado.includes('5j') ) ?
                ( (contador[0]+=2) && ' sus'+ String.fromCharCode(9839) +'4 3' ) :

            // acordes de potencia
            ( estado.includes('5j') && (estado.length == 2) ) ?
            ( (contador[0]++) && ' 5' ) :

            (() => {
                // throw 'valor en funcion analisis triada no aceptada';
                return '?';
            })()
        );
    // agrega los intervalos faltantes al nombre (en la primera octava)
    const posTriada = (intervalos, contador) => (

        // segunda menor
        salidaPostTriada(
            intervalos.includes('2m') &&
            intervalos.includes('5j') &&
            (contador[0]++) &&
            'min2'
        )
        // triadas normales con la segunda mayor
        + salidaPostTriada(
            intervalos.includes('2M') &&
            ( intervalos.includes('3M') || intervalos.includes('3m') ) &&
            (contador[0]++) &&
            'add2'
        )
        // triadas normales con la cuarta justa
        + salidaPostTriada(
            intervalos.includes('4j') &&
            ( intervalos.includes('3M') || intervalos.includes('3m') ) &&
            (contador[0]++) &&
            'add4'
        )
        // sesta mayores o septimas disminuidas
        + salidaPostTriada(
            intervalos.includes('6M') && (contador[0]++) && '6'
        )
        // sestas menores
        + salidaPostTriada(
            intervalos.includes('6m')  &&
            intervalos.includes('5j') &&
            (contador[0]++) &&
            'min6'
        )
        // septimas Mayores
        + salidaPostTriada(
            intervalos.includes('7M') &&
            intervalos.includes('3M') &&
            (contador[0]++) &&
            (intervalos.includes('5j') ?
                String.fromCharCode(9651) : 'maj7')
        )
        // septimas menores
        + salidaPostTriada(
            intervalos.includes('7m') && (contador[0]++) && '7'
        )
    );
    // agrega los intervalos faltantes al nombre a partir de la segunda octava
    const pt8vaAmpliada = (array_intervalos, contador) => ' ' +
        array_intervalos.filter(item => parseInt(item) > 7)
            .map(
                item => /\d+m$/g.test(item) ? ((contador[0]++) && `min${parseInt(item)}`) :   // menor
                        /\d+[Mj]$/g.test(item) ? ((contador[0]++) && `${parseInt(item)}`) :   // mayor o justa
                        /\d+a$/g.test(item) ? ((contador[0]++) && `aug${parseInt(item)}`) :   // aumentada
                        /\d+d$/g.test(item) ? ((contador[0]++) && `dim${parseInt(item)}`) :   // disminuida
                        ''
            ).join(' ');

    let nombre_salida = [];

    let conteo_intervalos_analisados = [1];
    let nombre =
        obj.base.split(':')[0] +                                             // nota base del acorde
        analisistriada(obj.intervalos, conteo_intervalos_analisados) +       // intervalos de la triada
        posTriada(obj.intervalos, conteo_intervalos_analisados) +            // intervalos de la primera octava
        pt8vaAmpliada(obj.intervalos, conteo_intervalos_analisados);          // demás intervalos
    // console.log(conteo_intervalos_analisados[0]);
    console.warn(nombre, obj);
    if (obj.intervalos.length == conteo_intervalos_analisados[0])
        nombre_salida = nombre.trim();
    else nombre_salida = '?';



  return nombre_salida;
}

/**
reacomodo de los valores (nombres) con tamañs de menor a mayor
@param {Array} array_nombres
@return {Array} de nombres
*/
function acomodoNombres(array_nombres) {
    if (array_nombres.length == 0) return ['?'];
    let texto = [...array_nombres].map(item=>item.trim());
    console.log(texto);
    // texto = texto.map(item => item.trim());
    for (var i = 0; i < texto.length; i++) {
        if (i != texto.length-1 && texto[i].split(' ').length > texto[i+1].split(' ').length) {
            console.info(texto[i],'|',texto[i+1],i);
            let tmp = texto[i];
            texto[i] = texto[i+1];
            texto[i+1] = tmp;
            i = -1;

        } else if ( i > 0 && texto[i].split(' ').length < texto[i-1].split(' ').length) {
            console.warn(texto[i],i,'o');
            let tmp = texto[i];
            texto[i] = texto[i-1];
            texto[i-1] = tmp;
            i = -1;

        } else {
            continue;
        }
    }

    console.info(texto);

    // reordenar de menor a mayor (con igual cantidad de partes)
    for (var i = 0; i < texto.length; i++) {

        if (i != texto.length-1 && texto[i].split(' ').length == texto[i+1].split(' ').length) {
            let cmp = [0,0];
            let vl = [texto[i].split(' '), texto[i+1].split(' ')];
            for (var o = 0; o < vl[0].length; o++) {
                cmp[0] += vl[0][o].length;
                cmp[1] += vl[1][o].length;
            }

            if (cmp[0] > cmp[1]) {
                let tmp = texto[i];
                texto[i] = texto[i+1];
                texto[i+1] = tmp;
                i = -1;
            }

        } else if (i != 0 && texto[i].split(' ').length == texto[i-1].split(' ').length) {
           let cmp = [0,0];
           let vl = [texto[i].split(' '), texto[i-1].split(' ')];
           for (var o = 0; o < vl[0].length; o++) {
               cmp[0] += vl[0][o].length;
               cmp[1] += vl[1][o].length;
           }

           if (cmp[0] < cmp[1]) {
               let tmp = texto[i];
               texto[i] = texto[i-1];
               texto[i-1] = tmp;
               i = -1;
           }

       }  else {
           continue;
       }

    }
    console.info(texto);
    return texto;
}

function acomodo(array_acorde) {
    var notas = [...array_acorde];
    for (var i = 0; i < notas.length; i++) {
        if (i != notas.length-1 && notas[i].split(':')[1] > notas[i+1].split(':')[1]) {
            var tmp = notas[i];
            notas[i] = notas[i+1];
            notas[i+1] = tmp;
            i = 0;

        } else if (i != 0 && notas[i].split(':')[1] < notas[i-1].split(':')[1]) {
            var tmp = notas[i];
            notas[i] = notas[i-1];
            notas[i-1] = tmp;
            i = 0;

        } else {
            continue;
        }
    }
    return notas;
}

function rmRepeatNotes(acorde) {
    // filtrar notas repetidas
    // if (!all_intervalos) {
        for (let a = 0; a < acorde.length; a++) {
            for (let b = 0; b < acorde.length; b++) {
                if (a==b) { continue; }
                if (acorde[a] == acorde[b]) {
                    acorde.splice(b,1);
                    break;
                }
            }
        }
    // } else if (acorde[0].includes(':')) {
    //     for (let a = 0; a < acorde.length; a++) {
    //         for (let b = 0; b < acorde.length; b++) {
    //             if (a==b) { continue; }
    //             if (acorde[a].split(':')[0] == acorde[b].split(':')[0]) {
    //                 acorde.splice(b,1);
    //                 b = -1;
    //                 // break;
    //             }
    //         }
    //     }
    // }

    return acorde;

}

const Acorde = (notas) => ({
        composicion : [...notas],
        base : notas[0],
        intervalos : getIntervalos([...notas]),
    });


function analisis(chord,all_intervalos=false) {

    let nombres_retorno;

    console.group('ConsChord | AnaChord');
    try {

        // validacion y conversion de datos
        if (!Array.isArray(chord) && typeof chord != typeof '')
            throw 'argument is not an Array or String';

        if (typeof chord == typeof '') chord = chord.split(',');

        if (!chord.every(note => /^(([a-g](is|es)?)|es)(\:\d+)?$/g.test(note)))
            throw 'armuments are not a list of musical notes';

            // verificación de bemoles a sostenidos y fis y bis
        chord = rmRepeatNotes( chord ).map( note =>
              (note.includes('eis'))
                ? note.includes(':')
                    ? 'f:'+note.split(':')[1]
                    : 'f'
            : (note.includes('bis'))
                ? note.includes(':')
                    ? 'c:'+note.split(':')[1]
                    : 'c'
            : (note.includes('es'))
                ? note.includes(':')
                    ? `${esToIs(note.split(':')[0])}:${note.split(':')[1]}`
                    : esToIs(note)
            : note
        );





        if (all_intervalos) {
            // acomodar nombres segun su longitud
            nombres_retorno = acomodoNombres(
                // get all invs
                invs( chord )
                    // por cada inversion
                    // crear objeto acorde
                    // analizar
                    // y adaptar el nombre a formato humano
                    .map( item => textoFiltrado( anaOne( Acorde( item ) ) ) )
                    // filtrar nombres extranos
                    .filter( item => !item.includes('?') )
            );

        } else {
            let tmp = textoFiltrado(
                anaOne(
                    Acorde( chord ) ) )
            if (!tmp.includes('?')) nombres_retorno = tmp;
            else nombres_retorno = '?';
        }
    }

    catch (e) { throw new Error(e); }
    finally { console.groupEnd(); }

    return nombres_retorno;
}

return ({
    inversiones: function(notes) {
        try {
            if (Array.isArray(notes))
                return invs(notes);
            else throw new Error('argumett is not an Array');
        } catch (e) {
            console.error(e);
        }
    },
    Acorde: Acorde,
    notesToName :     analisis,
    rmRepeatNotes : function (notes) {
        try {
            if (Array.isArray(notes))
                return rmRepeatNotes(notes);
            else throw new Error('argumett is not an Array');
        } catch (e) {
            console.error(e);
        }
    },
    tonesToInterval : tonesToInterval_,
});


})();

function analisis(acorde,all_intervalos=false) {
    return anachord.notesToName(acorde,all_intervalos);
}
