function anaTriada(ntriada,tTriada,ptriada) {
    let notas = [ntriada, ...(
        // triada
        (tTriada == 'M') ?
            [ DEF_NOMBRE_NOTAS[(
                DEF_NOMBRE_NOTAS.indexOf(ntriada) +
                4 ) % DEF_NOMBRE_NOTAS.length ],
            DEF_NOMBRE_NOTAS[(
                DEF_NOMBRE_NOTAS.indexOf(ntriada) +
                7 ) % DEF_NOMBRE_NOTAS.length ] ] :

        (tTriada == 'm') ?
            [ DEF_NOMBRE_NOTAS[(
                DEF_NOMBRE_NOTAS.indexOf(ntriada) +
                3 ) % DEF_NOMBRE_NOTAS.length ],
            DEF_NOMBRE_NOTAS[(
                DEF_NOMBRE_NOTAS.indexOf(ntriada) +
                7 ) % DEF_NOMBRE_NOTAS.length ] ] :

        (tTriada == 'disminuido') ?
            [ DEF_NOMBRE_NOTAS[(
                DEF_NOMBRE_NOTAS.indexOf(ntriada) +
                3 ) % DEF_NOMBRE_NOTAS.length ],
            DEF_NOMBRE_NOTAS[(
                DEF_NOMBRE_NOTAS.indexOf(ntriada) +
                6 ) % DEF_NOMBRE_NOTAS.length ] ] :

        (tTriada == 'aumentado') ?
            [ DEF_NOMBRE_NOTAS[(
                DEF_NOMBRE_NOTAS.indexOf(ntriada) +
                4 ) % DEF_NOMBRE_NOTAS.length ],
            DEF_NOMBRE_NOTAS[(
                DEF_NOMBRE_NOTAS.indexOf(ntriada) +
                8 ) % DEF_NOMBRE_NOTAS.length ] ] :

        (tTriada == 'm aug5') ?
            [ DEF_NOMBRE_NOTAS[(
                DEF_NOMBRE_NOTAS.indexOf(ntriada) +
                3 ) % DEF_NOMBRE_NOTAS.length ],
            DEF_NOMBRE_NOTAS[(
                DEF_NOMBRE_NOTAS.indexOf(ntriada) +
                8 ) % DEF_NOMBRE_NOTAS.length ] ] :

        (tTriada == '2sus') ?
            [ DEF_NOMBRE_NOTAS[(
                DEF_NOMBRE_NOTAS.indexOf(ntriada) +
                2 ) % DEF_NOMBRE_NOTAS.length ],
            DEF_NOMBRE_NOTAS[(
                DEF_NOMBRE_NOTAS.indexOf(ntriada) +
                7 ) % DEF_NOMBRE_NOTAS.length ] ] :

        (tTriada == '4sus') ?
            [ DEF_NOMBRE_NOTAS[(
                DEF_NOMBRE_NOTAS.indexOf(ntriada) +
                5 ) % DEF_NOMBRE_NOTAS.length ],
            DEF_NOMBRE_NOTAS[(
                DEF_NOMBRE_NOTAS.indexOf(ntriada) +
                7 ) % DEF_NOMBRE_NOTAS.length ] ] :

        (tTriada == '5') ?
            [ DEF_NOMBRE_NOTAS[(
                DEF_NOMBRE_NOTAS.indexOf(ntriada) +
                7 ) % DEF_NOMBRE_NOTAS.length ] ] :

        ['']
    )];

    if (ptriada != '') {
        ptriada.forEach(intervalo =>
            notas.push(
                (intervalo == 'min2') ?
                    DEF_NOMBRE_NOTAS[(
                        DEF_NOMBRE_NOTAS.indexOf(ntriada) +
                        1 ) % DEF_NOMBRE_NOTAS.length ] :

                (intervalo == 'add2') ?
                    DEF_NOMBRE_NOTAS[(
                        DEF_NOMBRE_NOTAS.indexOf(ntriada) +
                        2 ) % DEF_NOMBRE_NOTAS.length ] :

                (intervalo == 'add4') ?
                    DEF_NOMBRE_NOTAS[(
                        DEF_NOMBRE_NOTAS.indexOf(ntriada) +
                        5 ) % DEF_NOMBRE_NOTAS.length ] :

                (intervalo == 'min6') ?
                    DEF_NOMBRE_NOTAS[(
                        DEF_NOMBRE_NOTAS.indexOf(ntriada) +
                        8 ) % DEF_NOMBRE_NOTAS.length ] :

                (intervalo == '6') ?
                    DEF_NOMBRE_NOTAS[(
                        DEF_NOMBRE_NOTAS.indexOf(ntriada) +
                        9 ) % DEF_NOMBRE_NOTAS.length ] :

                (intervalo == '7m') ?
                    DEF_NOMBRE_NOTAS[(
                        DEF_NOMBRE_NOTAS.indexOf(ntriada) +
                        10 ) % DEF_NOMBRE_NOTAS.length ] :

                (intervalo == 'maj7') &&
                    DEF_NOMBRE_NOTAS[(
                        DEF_NOMBRE_NOTAS.indexOf(ntriada) +
                        11 ) % DEF_NOMBRE_NOTAS.length ]
            )
        );
    }

    // if (tTriada == 'M') {
    //     // tercera Mayor
    //     notas.push(
    //         DEF_NOMBRE_NOTAS[
    //             DEF_NOMBRE_NOTAS.indexOf(ntriada) +
    //             4
    //         ]
    //     )
    //     // quinta justa
    //     notas.push(
    //         DEF_NOMBRE_NOTAS[
    //             DEF_NOMBRE_NOTAS.indexOf(ntriada) +
    //             7
    //         ]
    //     )
    // }



    return notas;
}
